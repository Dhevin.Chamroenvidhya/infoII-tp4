class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        # Insertion de l'élément à la bonne position dans la file en fonction de sa priorité
        index = 0
        while index < len(self.items) and self.items[index][0] <= item[0]:
            index += 1
        self.items.insert(index, item)

    def dequeue(self, item=None):
        # Enlever l'élément soit à la dernière position si rien n'est précisé (None), ou alors en précisant le tuple
        if item is None:
            return self.items.pop()
        else:
            if item in self.items:
                self.items.remove(item)
                return item

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)

    def afficher(self):
        # Pas d'ajout supplémentaire dans is_empty et size, mais ajout d'une méthode pour afficher le résultat pour faire des tests
        return self.items

if __name__ == "__main__":
    #Instantiation de la classe et ajout pour faire des tests
    file = File()
    file.enqueue((4, "Nathan"))
    file.enqueue((2, "Sophie"))
    file.enqueue((1, "Amandine"))
    file.enqueue((3, "Julia"))
    # Le dequeue marche bien en indiquant Nathan et sa position ainsi qu'en indiquant rien du tout
    file.dequeue((4, "Nathan"))
    file.dequeue()
    print(file.size())
    print(file.is_empty())
    # J'affiche le résultat pour tester
    print(file.afficher())

